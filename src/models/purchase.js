import db from '../db/db';
import Sequelize from 'sequelize';

const Purchase = db.db.define('Purchases', {
    Id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1,
        notNull: true
    },
    Amount: Sequelize.DECIMAL(10, 2),
    Code: Sequelize.STRING(100),
    SupplierName: Sequelize.STRING(100),
    RegisterBy: Sequelize.STRING(100),
    UpdateBy: Sequelize.STRING(100),
    DestroyBy: Sequelize.STRING(100)
});

module.exports = Purchase