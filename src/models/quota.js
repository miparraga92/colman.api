import db from '../db/db';
import Sequelize from 'sequelize';

const Quota = db.db.define('Quotas', {
    Id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1,
        notNull: true
    },
    Amount: Sequelize.DECIMAL(10,2),
    QuotaNumber: Sequelize.INTEGER,
    PaymentDate: Sequelize.STRING(100),
    RegisterBy: Sequelize.STRING(100),
    UpdateBy: Sequelize.STRING(100),
    DestroyBy: Sequelize.STRING(100)
});

module.exports = Quota