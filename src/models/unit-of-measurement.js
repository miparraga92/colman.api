import db from '../db/db';
import Sequelize from 'sequelize';

const UnitOfMeasurement = db.db.define('UnitOfMeasurements', {
    Id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1,
        notNull: true
    },
    UnitOfMeasurement: Sequelize.STRING(100),
    RegisterBy: Sequelize.STRING(100),
    UpdateBy: Sequelize.STRING(100),
    DestroyBy: Sequelize.STRING(100)
});

module.exports = UnitOfMeasurement