import db from '../db/db';
import Sequelize from 'sequelize';

const Credit = db.db.define('Credits', {
    Id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1,
        notNull: true
    },
    Amount: Sequelize.DECIMAL(10, 2),
    AmountQuantity: Sequelize.DECIMAL(10, 2),
    FinancedAmount: Sequelize.DECIMAL(10,2),
    Residue: Sequelize.DECIMAL(10,2),
    Remainder: Sequelize.DECIMAL(10,2),
    QuotasQuantity: Sequelize.INTEGER,
    InterestRate: Sequelize.DECIMAL(5,2),
    InitialPaymentDate: Sequelize.STRING(100),
    FinalPaymentDate: Sequelize.STRING(100),
    RegisterBy: Sequelize.STRING(100),
    UpdateBy: Sequelize.STRING(100),
    DestroyBy: Sequelize.STRING(100)
});

module.exports = Credit