import db from '../db/db';
import Sequelize from 'sequelize';

const Plan = db.db.define('Plans', {
    Id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1,
        notNull: true
    },
    PlanName: Sequelize.STRING(100),
    QuotasQuantity: Sequelize.INTEGER,
    InterestRate: Sequelize.DECIMAL(5,2),
    RegisterBy: Sequelize.STRING(100),
    UpdateBy: Sequelize.STRING(100),
    DestroyBy: Sequelize.STRING(100)
});

module.exports = Plan