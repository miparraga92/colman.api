import db from '../db/db';
import Sequelize from 'sequelize';

const Person = db.db.define('Persons', {
    Id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1,
        notNull: true
    },
    FirstName: Sequelize.STRING(100),
    LastName: Sequelize.STRING(100),
    DocumentNumber: Sequelize.STRING(100),
    Sex: Sequelize.STRING(100),
    Address: Sequelize.STRING(100),
    PhoneNumber: Sequelize.STRING(100),
    Neighborhood: Sequelize.STRING(100),
    Email: Sequelize.STRING(100),
    RegisterBy: Sequelize.STRING(100),
    UpdateBy: Sequelize.STRING(100),
    DestroyBy: Sequelize.STRING(100)
});

module.exports = Person