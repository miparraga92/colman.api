import db from '../db/db';
import Sequelize from 'sequelize';

const Client = db.db.define('Clients', {
    Id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1,
        notNull: true
    },
    BusinessName: Sequelize.STRING(100),
    RegisterBy: Sequelize.STRING(100),
    UpdateBy: Sequelize.STRING(100),
    DestroyBy: Sequelize.STRING(100)
});

module.exports = Client