import db from '../db/db';
import Sequelize from 'sequelize';

const User = db.db.define('Users', {
    Id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1,
        notNull: true
    },
    User: Sequelize.STRING(100),
    Pass: Sequelize.STRING,
    RegisterBy: Sequelize.STRING(100),
    UpdateBy: Sequelize.STRING(100),
    DestroyBy: Sequelize.STRING(100)
});

module.exports = User