import db from '../db/db';
import Sequelize from 'sequelize';

const SaleDetail = db.db.define('SaleDetails', {
    Id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1,
        notNull: true
    },
    Quantity: Sequelize.INTEGER,
    RegisterBy: Sequelize.STRING(100),
    UpdateBy: Sequelize.STRING(100),
    DestroyBy: Sequelize.STRING(100)
});

module.exports = SaleDetail