import db from '../db/db';
import Sequelize from 'sequelize';

const MethodOfPayment = db.db.define('MethodOfPayments', {
    Id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1,
        notNull: true
    },
    MethodOfPayment: Sequelize.STRING(100),
    RegisterBy: Sequelize.STRING(100),
    UpdateBy: Sequelize.STRING(100),
    DestroyBy: Sequelize.STRING(100)
});

module.exports = MethodOfPayment