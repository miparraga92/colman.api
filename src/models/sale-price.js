import db from '../db/db';
import Sequelize from 'sequelize';

const SalePrice = db.db.define('SalePrices', {
    Id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1,
        notNull: true
    },
    Amount: Sequelize.DECIMAL(10, 2),
    RegisterBy: Sequelize.STRING(100),
    UpdateBy: Sequelize.STRING(100),
    DestroyBy: Sequelize.STRING(100)
});

module.exports = SalePrice