import db from '../db/db';
import Sequelize from 'sequelize';

const Product = db.db.define('Products', {
    Id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1,
        notNull: true
    },
    Name: Sequelize.STRING(100),
    MinimumStock: Sequelize.INTEGER,
    Brand: Sequelize.STRING(100),
    Description: Sequelize.STRING,
    InternalCode: Sequelize.STRING(100),
    RegisterBy: Sequelize.STRING(100),
    UpdateBy: Sequelize.STRING(100),
    DestroyBy: Sequelize.STRING(100)
});

module.exports = Product