import express from 'express';

import plans from '../controllers/plans';
import auth from '../controllers/auth';

const routes = express.Router();

// routes.route('/employees/:id')
    // .all(auth.verifyToken)
    // .get(employees.read)
    // .put(employees.update)
    // .delete(employees.delete);

routes.route('/plans')
    // .all(auth.verifyToken)
    .get(plans.all);

module.exports = routes;