import express from 'express';

import clients from '../controllers/clients';
import auth from '../controllers/auth';

const routes = express.Router();

routes.route('/clients/:id')
    // .all(auth.verifyToken)
    .get(clients.read)
    .put(clients.update)
    .delete(clients.delete);

routes.route('/clients')
    // .all(auth.verifyToken)
    .get(clients.all);

// routes.route('/')
//     .get(auth.verifyToken, clients.list)
//     .post(clients.create);

module.exports = routes;