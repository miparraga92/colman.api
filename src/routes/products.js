import express from 'express';

import products from '../controllers/products';
import auth from '../controllers/auth';

const routes = express.Router();

routes.route('/products/:id')
    // .all(auth.verifyToken)
    .get(products.read)
    .put(products.update)
    .delete(products.delete);

routes.route('/products')
    // .all(auth.verifyToken)
    .get(products.all)

// routes.route('/')
//     .get(auth.verifyToken, clients.list)
//     .post(clients.create);

module.exports = routes;