import express from 'express';

import auth from './auth';
import clients from './clients';
import employees from './employees';
import plans from './plans';
import methodOfPayments from './method-of-payments';
import response from '../helpers/response';
import products from './products';
const routes = express.Router();

routes.use(response.setHeadersForCORS);

routes.use('/', auth);
routes.use('/api', clients);
routes.use('/api', employees);
routes.use('/api', products);
routes.use('/api', plans);
routes.use('/api', methodOfPayments);

routes.get('/', (req, res) => {
  res.status(200).json({ message: 'Ok' });
});

routes.use(function (req, res) {
  response.sendNotFound(res);
});

module.exports = routes;