import express from 'express';

import employees from '../controllers/employees';
import auth from '../controllers/auth';

const routes = express.Router();

routes.route('/employees/:id')
    // .all(auth.verifyToken)
    .get(employees.read)
    .put(employees.update)
    .delete(employees.delete);

routes.route('/employees')
    // .all(auth.verifyToken)
    .get(employees.all);

module.exports = routes;