import express from 'express';

import methodOfPayments from '../controllers/method-of-payments';
import auth from '../controllers/auth';

const routes = express.Router();

// routes.route('/employees/:id')
    // .all(auth.verifyToken)
    // .get(employees.read)
    // .put(employees.update)
    // .delete(employees.delete);

routes.route('/method-of-payments')
    // .all(auth.verifyToken)
    .get(methodOfPayments.all);

module.exports = routes;