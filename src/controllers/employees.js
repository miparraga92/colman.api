import response from '../helpers/response';
import request from '../helpers/request';
import pagination from '../helpers/pagination';

import EmployeeRepository from '../repositories/employee-repository';


exports.read = function (req, res) {
    EmployeeRepository.findById(req.params.id, function (err, employee) {
        if (err) return response.sendNotFound(res);
        response.sendSuccess(res, employee)
    });
};

exports.all = function (req, res) {
    EmployeeRepository.getAll(function (err, employee) {
        if (err) return response.sendNotFound(res);
        response.sendSuccess(res, employee)
    });
};

exports.update = function (req, res) {
    const employee = req.body;
    EmployeeRepository.update(req.params.id, employee, function (err, result) {
        if (err) return response.sendBadRequest(res, err);
        response.sendSuccess(res, result);
    });
};

exports.delete = function (req, res) {
    EmployeeRepository.remove(req.params.id, function (err, result) {
        if (err) return response.sendBadRequest(res, err);
        response.sendSuccess(res, result);
    })
};