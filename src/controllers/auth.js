import jwt from 'jsonwebtoken';
import config from 'config';
import response from '../helpers/response';
import UserRepository from '../repositories/user-repository';

const privateKey = config.key.privateKey;
const tokenExpireInMinutes = config.key.tokenExpireInMinutes;

exports.authenticate = function (req, res) {
    UserRepository.verifyPassword(req.body.user, req.body.pass, function (err, result) {
        if (result) {
            const token = jwt.sign(result, privateKey, {
                expiresIn: tokenExpireInMinutes
            });

            response.sendSuccess(res, token, 'Token created.')
        } else {
            response.sendUnauthorized(res, 'Authentication failed.');
        }
    });
}

exports.verifyToken = function (req, res, next) {
    const token = req.body.token || req.query.token || req.headers['authorization'];

    if (token) {
        jwt.verify(token, privateKey, function (err, decoded) {
            if (err) {
                response.sendUnauthorized(res, 'Failed to authenticate token.');
            } else {
                UserRepository.getById(decoded.Id, function (err, user) {
                    if (err) res.send(err);
                    req.currentUser = user;
                    next();
                });
            }
        });
    } else {
        response.sendUnauthorized(res, 'No token provided.');
    }
};