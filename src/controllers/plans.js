import response from '../helpers/response';
import request from '../helpers/request';
import pagination from '../helpers/pagination';

import PlanRepository from '../repositories/plan-repository';

exports.all = function (req, res) {
    PlanRepository.getAll(function (err, plan) {
        if (err) return response.sendNotFound(res);
        response.sendSuccess(res, plan)
    });
};