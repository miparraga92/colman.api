import response from '../helpers/response';
import request from '../helpers/request';
import pagination from '../helpers/pagination';

import ClientRepository from '../repositories/client-repository';


exports.read = function (req, res) {
    ClientRepository.findById(req.params.id, function (err, client) {
        if (err) return response.sendNotFound(res);
        response.sendSuccess(res, client)
    });
};

exports.all = function (req, res) {
    ClientRepository.getAll(function (err, client) {
        if (err) return response.sendNotFound(res);
        response.sendSuccess(res, client)
    });
};


// exports.create = function (req, res) {
//     const newUser = new User(req.body);
//     newUser.role = 'user';
//     newUser.save(function (err, user) {
//         if (err) return response.sendBadRequest(res, err);
//         response.sendCreated(res, user);
//     });
// };

exports.update = function (req, res) {
    const client = req.body;
    ClientRepository.update(req.params.id, client, function (err, result) {
        if (err) return response.sendBadRequest(res, err);
        response.sendSuccess(res, result);
    });
};

exports.delete = function (req, res) {
    ClientRepository.remove(req.params.id, function (err, result) {
        if (err) return response.sendBadRequest(res, err);
        response.sendSuccess(res, result);
    })
};

// exports.loadUser = function (req, res, next) {
//     User.findById(req.params.userId, function (err, user) {
//         if (err) return response.sendNotFound(res);
//         if (!req.locals) req.locals = {};
//         req.locals.user = user;
//         next();
//     });
// };