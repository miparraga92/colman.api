import response from '../helpers/response';
import request from '../helpers/request';
import pagination from '../helpers/pagination';

import MethodOfPaymentRepository from '../repositories/method-of-payment-repository';

exports.all = function (req, res) {
    MethodOfPaymentRepository.getAll(function (err, methodOfPayment) {
        if (err) return response.sendNotFound(res);
        response.sendSuccess(res, methodOfPayment)
    });
};