import response from '../helpers/response';
import request from '../helpers/request';
import pagination from '../helpers/pagination';

import ProductRepository from '../repositories/product-repository';


exports.read = function (req, res) {
    ProductRepository.findById(req.params.id, function (err, product) {
        if (err) return response.sendNotFound(res);
        response.sendSuccess(res, product)
    });
};

exports.all = function (req, res) {
    ProductRepository.getAll(function (err, product) {
        if (err) return response.sendNotFound(res);
        response.sendSuccess(res, product)
    });
};

exports.update = function (req, res) {
    const product = req.body;
    ProductRepository.update(req.params.id, product, function (err, result) {
        if (err) return response.sendBadRequest(res, err);
        response.sendSuccess(res, result);
    });
};

exports.delete = function (req, res) {
    ProductRepository.remove(req.params.id, function (err, result) {
        if (err) return response.sendBadRequest(res, err);
        response.sendSuccess(res, result);
    })
};
