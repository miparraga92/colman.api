import db  from './db';
import Sequelize from 'sequelize';

import Client from '../models/client';
import Credit from '../models/credit';
import Employee from '../models/employee';
import FinancialState from '../models/financial-state';
import MethodOfPayment from '../models/method-of-payment';
import Person from '../models/person';
import Plan from '../models/plan';
import Product from '../models/product';
import PurchaseDetail from '../models/purchase-detail';
import PurchasePrice from '../models/purchase-price';
import Purchase from '../models/purchase';
import Quota from '../models/quota';
import Role from '../models/role';
import SaleDetail from '../models/sale-detail';
import SalePrice from '../models/sale-price';
import Sale from '../models/sale';
import Stock from '../models/stock';
import UnitOfMeasurement from '../models/unit-of-measurement';
import User from '../models/user';

Product.belongsTo(UnitOfMeasurement)
UnitOfMeasurement.hasMany(Product)

SalePrice.belongsTo(Product)
Product.hasMany(SalePrice)

PurchasePrice.belongsTo(Product)
Product.hasMany(PurchasePrice)

Stock.belongsTo(Product)
Product.hasMany(Stock)

User.belongsTo(Role)
Role.hasMany(User)

User.belongsTo(Person)
Client.belongsTo(Person)
Employee.belongsTo(Person)

Client.belongsTo(Employee)
Employee.hasMany(Client)

Purchase.belongsTo(FinancialState)
FinancialState.hasMany(Purchase)

PurchaseDetail.belongsTo(Purchase)
Purchase.hasMany(PurchaseDetail)

PurchaseDetail.belongsTo(Product)
Product.hasMany(PurchaseDetail)

Purchase.belongsTo(Employee)
Employee.hasMany(Purchase)

Sale.belongsTo(FinancialState)
FinancialState.hasMany(Sale)

SaleDetail.belongsTo(Sale)
Sale.hasMany(SaleDetail)

SaleDetail.belongsTo(Product)
Product.hasMany(SaleDetail)

Sale.belongsTo(Employee)
Employee.hasMany(Sale)

Credit.belongsTo(Sale)
Credit.belongsTo(Credit, {as: 'ParentCredit'})

Credit.belongsTo(FinancialState)
FinancialState.hasMany(Credit)

Quota.belongsTo(Credit)
Credit.hasMany(Quota)

Credit.belongsTo(Employee)
Employee.hasMany(Credit)

Credit.belongsTo(Employee, {as: 'Collector'})
Employee.hasMany(Credit)

Credit.belongsTo(Client)
Client.hasMany(Credit)

Credit.belongsTo(Plan)
Plan.hasMany(Credit)

Plan.belongsTo(MethodOfPayment)
MethodOfPayment.hasMany(Plan)

Credit.belongsTo(MethodOfPayment)
MethodOfPayment.hasMany(Credit)

// db.db.sync({ force: true })
// .then(() => {
//     console.log("Creada la base correctamentee!!");
//     console.log("Ejecutando seeds");
//     MethodOfPayment.create({ Id: 'MOP_DIARIO', MethodOfPayment: 'DIARIO' });
//     MethodOfPayment.create({ Id: 'MOP_SEMANAL', MethodOfPayment: 'SEMANAL' });
//     MethodOfPayment.create({ Id: 'MOP_MENSUAL', MethodOfPayment: 'MENSUAL' });
//     MethodOfPayment.create({ Id: 'MOP_QUINCENAL', MethodOfPayment: 'QUINCENAL' });
    
//     Plan.create({ Id: 'PLAN_CUSTOM', PlanName: 'PLAN PARTICULAR', QuotasQuantity: 0, InterestRate: 0, MethodOfPaymentId: 'MOP_DIARIO' });

//     console.log("Fin ejecución de seeds");
// })
exports.dbContext = db.db;