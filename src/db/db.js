import Sequelize from 'sequelize';
import config from 'config';

const db = new Sequelize(
    {
        operatorsAliases: false,
        host: config.database.url,
        dialect: config.database.dialect,
        database: config.database.database,
        username: config.database.user,
        password: config.database.pass,
        define: {
            underscored: false,
            charset: 'utf8',
            dialectOptions: {
                collate: 'utf8_general_ci'
            },
            timestamps: true,
            createdAt: 'RegisterDate',
            updatedAt: 'UpdateDate',
            deletedAt: 'DestroyDate',
            paranoid: true
        },
    }
);

db.authenticate().then(() => {
    console.log('Conexión extablecida con exito.');
})
.catch(err => {
    console.error('Hubo un problema al conectarse:', err);
});

exports.db = db;
