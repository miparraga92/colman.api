import Client from '../models/client';
import Person from '../models/person';

exports.findById = function (id, callback) {
    Client.findOne({
        where: {
            Id: id,
        }
    }).then(result => {
        if (result) {
            callback(null, result);
        } else {
            callback("Client not found.");
        }
    })
};

exports.getAll = function (callback) {
    Client.findAll({ include: [Person] })
        .then(result => {
            if (result) {
                callback(null, result);
            } else {
                callback("Client not found.");
            }
        }).catch(callback)
};

exports.remove = function (id, callback) {
    Client.findById(id).then(result => {
        if (!!result) {
            result.destroy().then(() => {
                callback(null, result)
            }).catch(callback)
        }else{
            callback('Client not found');
        }
    });
};

exports.update = function (id, client, callback) {
    Client.findById(id).then(result => {
        if (!!result) {
            Client.update(
                client,
                { where: { Id: id } }
            )
                .then(() => {
                    return Person.update(
                        client.Person,
                        { where: { Id: client.PersonId } }
                    )
                })
                .then(() => {
                    callback(null, client)
                })
        } else {
            Client.create(client, { include: [Person] }).then((newclient) => {
                if (!!newclient) {
                    callback(null, newclient)
                } else {
                    callback("Error al guardar el nuevo cliente")
                }
            })
        }
    })

    // return sequelize.transaction().then(function (t) {
    //     return User.create({
    //       firstName: 'Bart',
    //       lastName: 'Simpson'
    //     }, {transaction: t}).then(function (user) {
    //       return user.addSibling({
    //         firstName: 'Lisa',
    //         lastName: 'Simpson'
    //       }, {transaction: t});
    //     }).then(function () {
    //       return t.commit();
    //     }).catch(function (err) {
    //       return t.rollback();
    //     });
    //   });
};


