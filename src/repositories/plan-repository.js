import Plan from '../models/plan';

exports.getAll = function (callback) {
    Plan.findAll()
        .then(result => {
            if (result) {
                callback(null, result);
            } else {
                callback("Plan not found.");
            }
        }).catch(callback)
};


