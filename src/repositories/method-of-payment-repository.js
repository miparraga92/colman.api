import MethodOfPayment from '../models/method-of-payment';

exports.getAll = function (callback) {
    MethodOfPayment.findAll()
        .then(result => {
            if (result) {
                callback(null, result);
            } else {
                callback("MethodOfPayment not found.");
            }
        }).catch(callback)
};


