import Employee from '../models/employee';
import Person from '../models/person';

exports.findById = function (id, callback) {
    Employee.findOne({
        where: {
            Id: id,
        }
    }).then(result => {
        if (result) {
            callback(null, result);
        } else {
            callback("Employee not found.");
        }
    })
};

exports.getAll = function (callback) {
    Employee.findAll({ include: [Person] })
        .then(result => {
            if (result) {
                callback(null, result);
            } else {
                callback("Employee not found.");
            }
        }).catch(callback)
};

exports.remove = function (id, callback) {
    Employee.findById(id).then(result => {
        if (!!result) {
            result.destroy().then(() => {
                callback(null, result)
            }).catch(callback)
        } else {
            callback('Employee not found');
        }
    });
};

exports.update = function (id, employee, callback) {
    Employee.findById(id).then(result => {
        if (!!result) {
            Employee.update(
                employee,
                { where: { Id: id } }
            )
                .then(() => {
                    return Person.update(
                        employee.Person,
                        { where: { Id: employee.PersonId } }
                    )
                })
                .then(() => {
                    callback(null, employee)
                })
        } else {
            Employee.create(employee, { include: [Person] }).then((newEmployee) => {
                if (!!newEmployee) {
                    callback(null, newEmployee)
                } else {
                    callback("Error al guardar el nuevo empleado")
                }
            })
        }
    })
};


