import Product from '../models/product';
import SalePrice from '../models/sale-price';
import Stock from '../models/stock';


exports.findById = function (id, callback) {
    Product.findOne({
        where: {
            Id: id,
        }
    }).then(result => {
        if (result) {
            callback(null, result);
        } else {
            callback("Product not found.");
        }
    })
};

exports.getAll = function (callback) {
    Product.findAll({ include: [SalePrice, Stock] })
        .then(result => {
            if (result) {
                callback(null, result);
            } else {
                callback("Product not found.");
            }
        }).catch(callback)
};

exports.remove = function (id, callback) {
    Product.findById(id).then(result => {
        if (!!result) {
            result.destroy().then(() => {
                callback(null, result)
            }).catch(callback)
        }else{
            callback('Product not found');
        }
    });
};

exports.update = function (id, product, callback) {
    Product.findById(id).then(result => {
        if (!!result) {
            Product.update(
                product,
                { where: { Id: id } }
            )
                // .then(() => {
                //     return Product.update(
                //         client.Person,
                //         { where: { Id: client.PersonId } }
                //     )
                // })
                .then(() => {
                    callback(null, product)
                })
        } else {
            // , { include: [SalePrice, Stock] }
            Product.create(product).then((newproduct) => {
                if (!!newproduct) {
                    callback(null, newproduct)
                } else {
                    callback("Error al guardar el nuevo producto")
                }
            })
        }
    })

};
