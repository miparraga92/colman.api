import User from '../models/user';
import crypto from 'crypto';

exports.verifyPassword = function (user, pass, callback) {
    User.findOne({
        where: {
            User: user,
            Pass: crypto.createHash('md5').update(pass).digest("hex")
        }
    }).then(result => {
        if(result){
            var response = {
                Id: result.Id,
                User: result.User
            }
            callback(null, response);
        }else{
            callback("User not found.");
        }
    })
};

exports.getById = function (userId, callback) {
    User.findOne({
        where: {
            Id: userId,
        }
    }).then(result => {
        if(result){
            callback(null, result);
        }else{
            callback("User not found.");
        }
    })
};