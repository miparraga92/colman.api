import createError from 'http-errors';
import express from 'express';
import path from 'path';
import http from 'http';
import methodOverride from 'method-override';
import session from 'express-session';
import bodyParser from 'body-parser';
import multer from'multer';
import errorHandler from 'errorhandler';
import favicon from 'serve-favicon';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import config from 'config';
import dbContext from './db/dbContext';
import routes from './routes';
import cors from 'cors';

var app = express();
app.use(cors());
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(logger('dev'));
app.use(methodOverride());
app.use(bodyParser.json());
app.use(multer());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));

if ('dev' == app.get('env')) {
    app.use(errorHandler());
}

app.use('/', routes);

const port = process.env.PORT || config.server.port;
app.listen(port);
console.log('Node + Express REST API skeleton server started on port: ' + port);

module.exports = app;
