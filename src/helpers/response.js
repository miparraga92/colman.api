exports.sendCreated = function (res, data) {
    return res.status(201).send(
        {
            ResultCode: 1,
            ResultMsg: 'Successful request.',
            Result: data
        }
    );
};

exports.sendBadRequest = function (res, message) {
    return res.status(400).send(
        {
            ResultCode: 0,
            ResultMsg: message
        }
    );
};

exports.sendUnauthorized = function (res, message) {
    return res.status(401).send(
        {
            ResultCode: 0,
            ResultMsg: message
        }
    );
};

exports.sendForbidden = function (res) {
    return res.status(403).send(
        {
            ResultCode: 0,
            ResultMsg: 'You do not have rights to access this resource.'
        }
    );
};

exports.sendNotFound = function (res) {
    return res.status(404).send(
        {
            ResultCode: 0,
            ResultMsg: 'Resource not found.'
        }
    );
};

exports.sendSuccess = function (res, data, message) {
    return res.json(
        {
            ResultCode: 1,
            ResultMsg: message || 'Successful request.',
            Result: data
        }
    );
};

exports.setHeadersForCORS = function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, X-Access-Token, Content-Type, Accept");
    next();
}